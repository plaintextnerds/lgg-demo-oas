SHELL:=$(PREFIX)/bin/sh
TAG?=v0.0.0-local
VERSION:=$(shell npx --yes semver $(TAG))
PACKAGE_NAME:=lgg-demo-oas

build: \
	out/static/version.txt \
	out/static/openapi.yaml \
	out/static/openapi.json \
	out/static/index.html \
	out/npm/ \

rebuild: clean build

clean:
	rm -rf bin
	rm -rf out

out/static/version.txt:
	@mkdir --parents $(@D)
	echo $(VERSION) > $@

out/static/openapi.yaml: src/openapi.yaml
	@mkdir --parents $(@D)
	sed 's/0.0.0-local/$(VERSION)/' $< > $@

out/static/openapi.json: out/static/openapi.yaml
	@mkdir --parents $(@D)
	npx --yes js-yaml $< > $@

out/static/index.html: out/static/openapi.yaml
	@mkdir --parents $(@D)
	npx --yes redoc-cli bundle $< --output $@

out/npm/: out/static/openapi.yaml
	npx --yes oas3ts-generator@1.1.5 package \
		--package-dir $@ \
		--package-name @latency.gg/$(PACKAGE_NAME) \
		--request-type application/json \
		--response-type application/json \
		--response-type text/plain \
		$<
	( cd $@ ; npm install )


.PHONY: \
	build \
	rebuild \
	clean \
